
#include <ctime>
#include <chrono>

#include "dumbLog.h"
#include "types.h"


void dumbLog::setOutputStream(std::ostream* out)
{
	this->OUT = out;
}

const char*
dumbLog::logLevelToString(log_level level)
{
	switch (level) {
		case log_level::Error: return "Error";
		case log_level::Warning: return "Warning";
		case log_level::Info: return "Info";
		case log_level::Debug: return "Debug";
		default: return "Potato";
	}
};

std::tm*
dumbLog::getCurrentTime()
{
	auto now = std::chrono::system_clock::now();
	auto t_c = std::chrono::system_clock::to_time_t(now);
	return std::localtime(&t_c);
}

int
dumbLog::getCurrentMS()
{
	auto now = std::chrono::system_clock::now();
	u64 total_ms = std::chrono::duration_cast<std::chrono::milliseconds>(
		now.time_since_epoch()
	).count();
	return int(total_ms % 1000);
}


#include <cstdarg>
#include <ctime>


void
dumbLogF(log_level l, const char* func, const char* fmt, ...)
{
	const char* level = logger.logLevelToString(l);
	char time_str[100];
	timespec ts;
	timespec_get(&ts, TIME_UTC);
	i64 ms = ts.tv_nsec / 1000000;

	if (strftime(time_str, sizeof(time_str), "%T", localtime(&ts.tv_sec))) {
		// NOTE: print prefix, "H:M:S.ms, log_level, function(), "
		printf("%s.%03ld [%s] %s(), ", time_str, ms, level, func);

		// NOTE: append user args
		va_list args;
		va_start(args, fmt);
		vprintf(fmt, args);
		va_end(args);
	} else {
		printf("%s(), error getting time\n", __FUNCTION__);
	}
}
