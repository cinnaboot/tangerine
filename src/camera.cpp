
#include <cassert>

#include <GL/glew.h>
#define GLM_FORCE_XYZW_ONLY
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "camera.h"


// TODO: add these props to scene json
#define MOVE_SPEED 5.f
#define ROTATE_SPEED 0.005f
#define CAMERA_Z_CLAMP_ANGLE 85.f
#define FOV 60.f
#define NEAR_CLIP_PLANE 5.f


// forward declarations


// interface

void
cameraInitPerspective(Camera* cam,
	vec3 position,
	vec3 target,
	vec3 world_up,
	float aspect_ratio)
{
	assert(aspect_ratio > 0);

	cam->position = position;
	cam->target = target;
	cam->world_up = world_up;
	cam->xforms.projection = glm::infinitePerspective(glm::radians(FOV),
		aspect_ratio,
		NEAR_CLIP_PLANE);

	cam->forward = glm::normalize(target - position);
	cam->left = glm::normalize(glm::cross(cam->world_up, cam->forward));
	cam->up = glm::normalize(glm::cross(cam->forward, cam->left));

	cam->hAngle = glm::atan(cam->forward.x, cam->forward.y);
	// NOTE: get absolute value of relative axis for vAngle component
	float len = glm::sqrt(glm::pow(cam->forward.y, 2) +
		glm::pow(cam->forward.x, 2));
	cam->vAngle = glm::atan(cam->forward.z, len);

	cam->xforms.view=
		glm::lookAt(cam->position, cam->position + cam->forward, cam->up);
}

// TODO: re-add orthographic camera
void
cameraInitOrthographic(/*camera& cam, */)
{
#if 0
	// left, right, bottom, top, zNear, zFar
	cam.projection = glm::ortho(0.f, 1280.0f, 0.f, 720.0f, 0.1f, 100.0f);
	cam.view = glm::lookAt(
		vec3(0.0f, 0.0f, 1.0f),	// camera position
		vec3(0.0f, 0.0f, 0.0f),	// look at position
		vec3(0,1,0)				// "up" vector
	);

	cam.model = mat4(1.0f);
	cam.MVP = cam.projection * cam.view * cam.model;
#endif
}

vec2
cameraUnproject(Camera& cam, int x, int y, int vp_width, int vp_height)
{
#if 0
	// NOTE: using depth buffer may not be as accurate as doing ray-cast
	GLfloat depth;
	glReadPixels(x, y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &depth);
	vec4 viewport = vec4(0, 0, vp_width, vp_height);
	vec3 wincoord = vec3(x, y, depth);
	vec3 vU = glm::unProject(wincoord, cam.view, cam.projection, viewport);

	return vec2(vU.x, vU.y);
#endif
	return vec2();
}

vec3
cameraCreateRay(Camera& cam, ivec2 vp_coords, ivec2 vp_dims)
{
#if 0
	// NOTE: http://antongerdelan.net/opengl/raycasting.html
	float x = 2.f * vp_coords.x / vp_dims.x - 1.f;
	float y = 2.f * vp_coords.y / vp_dims.y - 1.f;
	vec4 ray_clip = vec4(x, y, -1.f, 1.f);
	vec4 ray_eye = glm::inverse(cam.projection) * ray_clip;
	ray_eye = vec4(ray_eye.x, ray_eye.y, -1.f, 0); // NOTE: reset as ray
	vec4 ray_world = glm::normalize(glm::inverse(cam.view) * ray_eye);

	return vec3(ray_world.x, ray_world.y, ray_world.z);
#endif
	return vec3();
}

bool
cameraIntersectPlane(Camera& cam,
	vec3 ray,
	vec3 plane_origin,
	vec3 plane_normal,
	vec3& intersection)
{
	// NOTE: https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-plane-and-ray-disk-intersection
	float divisor = glm::dot(ray, plane_normal);

	if (divisor <= 0.000001f && divisor >= -0.000001f) // NOTE: ray and plane are co-planar
		return false;

	float distance =
		glm::dot((plane_origin - cam.position), plane_normal) / divisor;
	vec3 xsect = cam.position + (ray * distance);
	intersection = vec3(xsect.x, xsect.y, xsect.z);

	return true;
}

void
cameraMove(Camera& cam,
	bool up,
	bool left,
	bool down,
	bool right,
	bool forward,
	bool backward)
{
	if (!up && !left && !down && !right && !forward && !backward)
		return;

	vec3 f = cam.forward;
	vec3 u = cam.up;
	vec3 old = cam.position;
	vec3 &p = cam.position;
	vec3 v(0.f); // normalized direction

	// TODO: still seems like we're adding magnitude when moving in 2 directions
#if 0
	if (forward) v = glm::normalize(v + f);
	if (backward) v = glm::normalize(v - f);
	if (up) v = glm::normalize(v + u);
	if (down) v = glm::normalize(v - u);
	if (left) v -= glm::normalize(glm::cross(f, u));
	if (right) v -= glm::normalize(glm::cross(u, f));
#else
	if (forward) v +=  f;
	if (backward) v -= f;
	if (up) v += u;
	if (down) v -= u;
	if (left) v -= glm::cross(f, u);
	if (right) v -= glm::cross(u, f);
#endif

	p += (v * MOVE_SPEED);
	vec3 diff = old - p;
	cam.xforms.view = glm::translate(cam.xforms.view, diff);
	//cam.view = glm::translate(cam.view, diff);
	//cam.MVP = cam.projection * cam.view * cam.model;
}

void
cameraRotate(Camera& cam, i32	xrel, i32 yrel)
{
	float &h = cam.hAngle;
	float &v = cam.vAngle;
	h += ROTATE_SPEED * xrel;
	v -= ROTATE_SPEED * yrel;

	// clamp vAngle to prevent gimbal lock
	float a = glm::radians(CAMERA_Z_CLAMP_ANGLE);
	if (v < (-1 * a)) v = (-1 * a);
	if (v > a) v = a;

	cam.forward = vec3(
		glm::cos(v) * glm::sin(h),
		glm::cos(v) * glm::cos(h),
		glm::sin(v)
	);

	glm::normalize(cam.forward);
	cam.left = glm::normalize(glm::cross(cam.forward, cam.world_up));
	cam.up = glm::normalize(glm::cross(cam.left, cam.forward));

	cam.xforms.view = glm::lookAt(cam.position,
								cam.position + cam.forward,
								cam.up);
	//cam.MVP = cam.projection * cam.view * cam.model;
}

void
cameraRoll(Camera& cam, bool CW, bool CCW)
{
#if 0
	if ((!CW && !CCW) || (CW && CCW))
		return;

	float a = 0.005f;
	if (CW) a *= 1;
	if (CCW) a *= -1;
	mat4 m = glm::rotate(mat4(1.f), a, cam.forward);
	vec4 v(cam.up.x, cam.up.y, cam.up.z, 0);
	v = v * m;
	cam.up = vec3(v.x, v.y, v.z);
	cam.view *= m;
	cam.MVP = cam.projection * cam.view * cam.model;
#endif
}

// internal

