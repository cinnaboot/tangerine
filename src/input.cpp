
#include "input.h"


void
inputProcessEvent(InputState* is, SDL_Event& e)
{
	switch (e.type) {
		case SDL_QUIT:
			is->window_closed = true;
			break;
		case SDL_KEYDOWN:
			switch (e.key.keysym.sym) {
				case SDLK_ESCAPE: is->escape = true; break;
				case SDLK_LEFT: is->left = true; break;
				case SDLK_RIGHT: is->right = true; break;
				case SDLK_UP: is->up = true; break;
				case SDLK_DOWN: is->down = true; break;
			}
			break;
		case SDL_KEYUP:
			switch (e.key.keysym.sym) {
				case SDLK_ESCAPE: is->escape = false; break;
				case SDLK_LEFT: is->left = false; break;
				case SDLK_RIGHT: is->right = false; break;
				case SDLK_UP: is->up = false; break;
				case SDLK_DOWN: is->down = false; break;
			}
			break;
		default: break;
	}
}

void
inputProcessEvents(InputState* is)
{
	SDL_Event e;

	while (SDL_PollEvent(&e))
		inputProcessEvent(is, e);
}

