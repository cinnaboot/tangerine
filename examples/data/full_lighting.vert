#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 uv;

out vec3 frag_pos;
out vec3 frag_normal;
out vec2 frag_uv;

layout (std140) uniform matrices
{
	mat4 view_xform;
	mat4 proj_xform;
	mat4 normal_xform;
};

uniform mat4 node_xform;


void main()
{
	frag_pos = (node_xform * vec4(position, 1)).xyz;
	frag_uv = uv;
	frag_normal = normalize(mat3(node_xform) * normal);
	gl_Position = proj_xform * view_xform * node_xform * vec4(position, 1);
}
