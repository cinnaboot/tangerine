
SHELL = /bin/sh
CXX = g++
# NOTE: have to set -O or -O1 here to get warnings for uninitialized variables
CXXFLAGS = -std=c++11 -g -ggdb3 -Wall -O -Wall -Wuninitialized \
	-Iinclude \
	-I/usr/include/SDL2 \
	-Iext/stb_libs \
	-Iext/tinygltf
EXAMPLEDIR = examples
OBJDIR = build
SRCDIR = src
LIBNAME = libTangerine.a

SOURCES = $(wildcard $(SRCDIR)/*.cpp)
OBJECTS = $(patsubst $(SRCDIR)/%.cpp, $(OBJDIR)/%.o, $(SOURCES))
NDBG_SOURCES = $(wildcard $(SRCDIR)/*.cc)
NDBG_OBJS = $(patsubst $(SRCDIR)/%.cc, $(OBJDIR)/%.o, $(NDBG_SOURCES))


all: mkdirs $(LIBNAME)
	$(MAKE) examples
.PHONY: all

-include $(OBJDIR)/*.d

mkdirs:
	@mkdir -p $(OBJDIR)
.PHONY: mkdirs

$(LIBNAME): $(OBJECTS) $(NDBG_OBJS)
	ar -crsuU $(OBJDIR)/$(LIBNAME) $(OBJECTS) $(NDBG_OBJS)

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) -c -MMD $< -o $@

# FIXME: re-using CXXFLAGS here defats the purpose of separating out NDBG_OJBS
# 	see shader_testing Makefile
$(NDBG_OBJS): $(OBJDIR)/%.o : $(SRCDIR)/%.cc
	$(CXX) $(CXXFLAGS) -c $< -o $@
	strip -d $@

examples:
	$(MAKE) -C $(EXAMPLEDIR)
.PHONY: examples

clean:
	rm -rf $(OBJDIR)/*
	$(MAKE) clean -C $(EXAMPLEDIR)
.PHONY: clean
