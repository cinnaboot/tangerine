
#pragma once

#include <iomanip>
#include <iostream>


enum log_level {
	Error,
	Warning,
	Info,
	Debug
};

struct dumbLog
{
	std::ostream* OUT = &std::cout;
	void setOutputStream(std::ostream* out);
	const char* logLevelToString(log_level level);
	std::tm* getCurrentTime();
	int getCurrentMS();
};

static dumbLog logger;

#define LOG(level) *logger.OUT \
	<< std::put_time(logger.getCurrentTime(), "%F %T.") \
		<< logger.getCurrentMS() << " " \
	<< "[" << logger.logLevelToString(level) << "] " \
	<< "(" << __FUNCTION__ << ") "


#include <cstdio>

#define LOGF(level, format_str, ...) \
	dumbLogF(level, __FUNCTION__, format_str, ##__VA_ARGS__);
void dumbLogF(log_level l, const char* func, const char* fmt, ...);
