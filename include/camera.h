
#pragma once

#define GLM_FORCE_XYZW_ONLY
#include <glm/glm.hpp>

#include "types.h"
#include "shader.h"


struct Transforms
{
	mat4 view;
	mat4 projection;
	mat4 normal;
};

struct Camera
{
	float hAngle;
	float vAngle;

	vec3 position;
	vec3 forward;
	vec3 up;
	vec3 left;
	vec3 target;
	vec3 world_up;

	Transforms xforms;
};


// FIXME: we should keep to our convention of passing pointers to structs in
// 	these interface functions
vec2
cameraUnproject(Camera& cam, int x, int y, int vp_width, int vp_height);

vec3
cameraCreateRay(Camera& cam, ivec2 vp_coords, ivec2 vp_dims);

bool
cameraIntersectPlane(Camera& cam,
	vec3 ray,
	vec3 plane_origin,
	vec3 plane_normal,
	vec3& intersection);

void
cameraInitPerspective(Camera* cam,
	vec3 position,
	vec3 target,
	vec3 world_up,
	float aspect_ratio = 16.f / 9.f);

void
cameraMove(Camera& cam,
	bool up,
	bool left,
	bool down,
	bool right,
	bool forward,
	bool backward);

void
cameraRotate(Camera& cam, i32	xrel, i32 yrel);

void
cameraRoll(Camera& cam, bool CW, bool CCW);

