
#pragma once

#include <SDL2/SDL.h>


struct InputState
{
	bool window_closed;
	bool escape;
	bool left;
	bool right;
	bool up;
	bool down;
};

void
inputProcessEvent(InputState* is, SDL_Event& e);

// NOTE: convenience function that provides a while(SDL_PollEvents()) loop
void
inputProcessEvents(InputState* is);

