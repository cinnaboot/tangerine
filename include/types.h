
#pragma once

#include <cstdint>
#define GLM_FORCE_XYZW_ONLY
#include <glm/glm.hpp>


typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int32_t i32;
typedef int64_t i64;

using glm::ivec2;
using glm::uvec2;
using glm::uvec4;
using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat4;

