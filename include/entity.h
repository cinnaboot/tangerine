
#pragma once

#include "asset.h"
#include "shader.h"
#include "types.h"
#include "util.h"


struct Entity
{
	u32 num_meshes;
	GLMesh* meshes;
	GLTexture* diffuse_texture;	// NOTE: pointer into gl_ctx->textures array
	mat4* model_xform;
	char* name;

	GLenum draw_mode; // NOTE: GL_LINES, GL_TRIANGLES (default)
	GLenum usage; // NOTE: GL_STATIC_DRAW (default), GL_DYNAMIC_DRAW
};


bool initEntity(Entity* e,
	GLContext* gl_ctx,
	MemoryArena* arena,
	Model* mdl,
	u32 num_attrib_mappings,
	GLBufferToAttribMapping* attrib_mappings,
	const char* name = "",
	GLenum draw_mode = GL_TRIANGLES,
	GLenum usage = GL_STATIC_DRAW);

void setEntityPosition(Entity* e, vec3 pos);

void rotateEntity(Entity* e, vec3 axis, float radians);

void scaleEntity(Entity* e, float scale);
